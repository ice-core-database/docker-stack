# Ice Core Database Docker Stack

## Structure

There are three repositories used for transforming data to bring into the database.

 - [IC CFA ICPMS](https://gitlab.com/ice-core-database/parse-ic-cfa-icpms)
 - [LAICPMS](https://gitlab.com/ice-core-database/parse-laicpms)
 - [SEM-EDAX](https://gitlab.com/ice-core-database/SEM-EDAX)

There is one repository for analysis. More will be added.

- [Pb Analysis](https://gitlab.com/ice-core-database/north-pacific-pb-emissions)

## Services

This repository contains the structures to create the following services, and a PGAdmin backup file to restore database structure.

 - Postgres [port 54320]
 - PGAdmin [port 54321]
 - Metabase [port 54322]

## Database Structure

core --> tube --> sample --> instrument data -- (if applicable) instrument metadata

### core
    - core_id (PKEY)
    - core_name

### tube
    - tube_id (PKEY)
    - core_id --> core.core_id
    - tube_name
    - depth_top
    - depth_bbottom
    - length
    - core_diameter_cm
    - density
    - top_we

### sample
    - sample_id (PKEY)
    - tube_id --> tube.tube_id
    - core_id --> core.core_id
    - depth_top
    - depth_bottom
    - depth_avg
    - age_top
    - age_bottom
    - age_avg
    - time_top
    - time_bottom
    - time_avg
    - sample_sanem


### cfa
    - cfa_id (PKEY)
    - sample_id --> sample.sample_id
    - electrical_conductivity
    - fine_particles
    - coarse_particles
    - particles_counts_1um
    - particles_counts_2um
    - particles_counts_3um
    - particles_counts_4um
    - particles_counts_5um
    - particles_counts_6um
    - particles_counts_7um
    - particles_counts_8um
    - particles_counts_9um
    - particles_counts_10um
    - flow_rate

### ic
    - ic_id (PKEY)
    - sample_id --> sample.sample_id
    - MSA_ppt
    - Cl_ppt
    - NO3_2_ppt
    - SO4_2_ppt
    - Na_ppt
    - NH4_ppt
    - Ca2_ppt

### icpms
    - icpms_id
    - sample_id --> sample.sample_id
    - Sr88_LR_ppt
    - Pb208_LR_ppt
    - As75_LR_ppt
    - AS75_HR_ppt
    - Cs133_LR_ppt
    - Ba137_LR_ppt
    - La139_LR_ppt
    - Ce140_LR_ppt
    - Pr141_LR_ppt
    - Li7_LR_ppt
    - V51_MR_ppt
    - Cr52_MR_ppt
    - MN55_MR_ppt
    - Co59_MR_ppt
    - Si28_MR_ppt
    - combo_Cu_ppt
    - combo_Cd_ppt
    - combo_Bi_ppt
    - combo_Zn_ppt
    - S32_MR_ppt
    - Ca44_MR_ppt
    - Fe56_MR_ppt
    - Na23_MR_ppt
    - K39_HR_ppt

### iso
    - iso_id (PKEY)
    - sample_id --> sample.sample_id
    - vial
    - resolution
    - x180
    - deuterium
    - d_excess

## laicpms_chemistry
    - laicpms_id (PKEY)
    - laicpms_run_id --> laicpms_instrument_metadata.laicpms_run_id
    - sample_id --> sample.sample_id
    - element
    - intensity
    - intensity_adjusted
    - flag_run (bool)
    - instrument_time
    - resolution (HR/LR)

## laicpms_instrument_metatdata [TODO - expand this]
    - laicpms_run_id (PKEY)
    - run_name

## sem_chemistry
    - sem_chem_id (PKEY)
    - sem_im_id --> sem_instrument_metadata.sem_im_id
    - sample_id --> sample.sample_id
    - element
    - wt_percent
    - mol_percent
    - z
    - a
    - f

## sem_instrument_metadata
    - sem_im_id (PKEY)
    - sample_id --> sample.sample_id
    - method
    - ref_dt
    - label
    - nrm_percent
    - kv
    - tilt
    - takeoff
    - detector_type
    - res
    - ampt
    - lsec
    - fs
    - date
    - quantification_method
    - quantification_standard
    - quantification_type
    - sem_user
