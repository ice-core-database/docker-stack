PGDMP         7            	    z            denali    14.3 (Debian 14.3-1.pgdg110+1)    14.3 )    A           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            B           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            C           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            D           1262    18099    denali    DATABASE     Z   CREATE DATABASE denali WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';
    DROP DATABASE denali;
                hbrooks    false            �            1259    18100    cfa    TABLE     �  CREATE TABLE public.cfa (
    sample_id integer NOT NULL,
    cfa_id integer NOT NULL,
    electrical_conductivity double precision,
    fine_particles double precision,
    coarse_particles double precision,
    particle_counts_1um double precision,
    particle_counts_2um double precision,
    particle_counts_3um double precision,
    particle_counts_4um double precision,
    particle_counts_5um double precision,
    particle_counts_6um double precision,
    particle_counts_7um double precision,
    particle_counts_8um double precision,
    particle_counts_9um double precision,
    particle_counts_10um double precision,
    flow_rate double precision
);
    DROP TABLE public.cfa;
       public         heap    hbrooks    false            �            1259    18129    core    TABLE     a   CREATE TABLE public.core (
    core_name character(50) NOT NULL,
    core_id integer NOT NULL
);
    DROP TABLE public.core;
       public         heap    hbrooks    false            �            1259    18103    ic    TABLE     w  CREATE TABLE public.ic (
    sample_id integer NOT NULL,
    ic_id integer NOT NULL,
    "MSA_ppt" double precision,
    "Cl_ppt" double precision,
    "NO3_2_ppt" double precision,
    "SO4_2_ppt" double precision,
    "Na_ppt" double precision,
    "NH4_ppt" double precision,
    "K_ppt" double precision,
    "Mg2_ppt" double precision,
    "Ca2_ppt" double precision
);
    DROP TABLE public.ic;
       public         heap    hbrooks    false            �            1259    18106    icpms    TABLE     �  CREATE TABLE public.icpms (
    sample_id integer NOT NULL,
    icpms_id integer NOT NULL,
    "Sr88_LR_ppt" double precision,
    "Pb208_LR_ppt" double precision,
    "As75_LR_ppt" double precision,
    "As75_HR_ppt" double precision,
    "Cs133_LR_ppt" double precision,
    "Ba137_LR_ppt" double precision,
    "La139_LR_ppt" double precision,
    "Ce140_LR_ppt" double precision,
    "Pr141_LR_ppt" double precision,
    "Li7_LR_ppt" double precision,
    "V51_MR_ppt" double precision,
    "Cr52_MR_ppt" double precision,
    "Mn55_MR_ppt" double precision,
    "Co59_MR_ppt" double precision,
    "Si28_MR_ppt" double precision,
    "combo_Cu_ppt" double precision,
    "combo_Cd_ppt" double precision,
    "combo_Bi_ppt" double precision,
    "combo_Zn_ppt" double precision,
    "S32_MR_ppt" double precision,
    "Ca44_MR_ppt" double precision,
    "Fe56_MR_ppt" double precision,
    "Na23_MR_ppt" double precision,
    "K39_HR_ppt" double precision
);
    DROP TABLE public.icpms;
       public         heap    hbrooks    false            �            1259    18109    iso    TABLE     �   CREATE TABLE public.iso (
    sample_id integer NOT NULL,
    vial character(50),
    resolution double precision,
    x180 double precision,
    deuterium double precision,
    d_excess double precision,
    iso_id integer NOT NULL
);
    DROP TABLE public.iso;
       public         heap    hbrooks    false            �            1259    20178    laicpms_chemistry    TABLE     [  CREATE TABLE public.laicpms_chemistry (
    laicpms_id integer NOT NULL,
    laicpms_run_id integer NOT NULL,
    sample_id integer NOT NULL,
    element character(50),
    intensity double precision,
    intensity_adjusted double precision,
    flag_run boolean DEFAULT false,
    instrument_time double precision,
    resolution character(2)
);
 %   DROP TABLE public.laicpms_chemistry;
       public         heap    hbrooks    false            �            1259    18115    laicpms_instrument_metadata    TABLE     u   CREATE TABLE public.laicpms_instrument_metadata (
    laicpms_run_id integer NOT NULL,
    run_name character(50)
);
 /   DROP TABLE public.laicpms_instrument_metadata;
       public         heap    hbrooks    false            �            1259    20173    lock_monitor    VIEW     �  CREATE VIEW public.lock_monitor AS
 SELECT COALESCE(((blockingl.relation)::regclass)::text, blockingl.locktype) AS locked_item,
    (now() - blockeda.query_start) AS waiting_duration,
    blockeda.pid AS blocked_pid,
    blockeda.query AS blocked_query,
    blockedl.mode AS blocked_mode,
    blockinga.pid AS blocking_pid,
    blockinga.query AS blocking_query,
    blockingl.mode AS blocking_mode
   FROM (((pg_locks blockedl
     JOIN pg_stat_activity blockeda ON ((blockedl.pid = blockeda.pid)))
     JOIN pg_locks blockingl ON ((((blockingl.transactionid = blockedl.transactionid) OR ((blockingl.relation = blockedl.relation) AND (blockingl.locktype = blockedl.locktype))) AND (blockedl.pid <> blockingl.pid))))
     JOIN pg_stat_activity blockinga ON (((blockingl.pid = blockinga.pid) AND (blockinga.datid = blockeda.datid))))
  WHERE ((NOT blockedl.granted) AND (blockinga.datname = current_database()));
    DROP VIEW public.lock_monitor;
       public          hbrooks    false            �            1259    18132    sample    TABLE     �  CREATE TABLE public.sample (
    tube_id integer NOT NULL,
    sample_id integer NOT NULL,
    depth_top double precision,
    depth_bottom double precision,
    depth_avg double precision,
    age_top double precision,
    age_bottom double precision,
    age_avg double precision,
    time_top double precision,
    time_bottom double precision,
    time_avg double precision,
    sample_name character(50) NOT NULL,
    core_id integer NOT NULL
);
    DROP TABLE public.sample;
       public         heap    hbrooks    false            �            1259    18118    sem_chemistry    TABLE     .  CREATE TABLE public.sem_chemistry (
    sem_chem_id integer NOT NULL,
    sem_im_id integer NOT NULL,
    sample_id integer NOT NULL,
    element character(50),
    wt_percent double precision,
    mol_percent double precision,
    z double precision,
    a double precision,
    f double precision
);
 !   DROP TABLE public.sem_chemistry;
       public         heap    hbrooks    false            �            1259    18121    sem_instrument_metadata    TABLE     h  CREATE TABLE public.sem_instrument_metadata (
    sem_im_id integer NOT NULL,
    sample_id integer NOT NULL,
    method character(50),
    ref_dt character(50),
    label character(50),
    nrm_percent character(50),
    kv double precision,
    tilt double precision,
    takeoff double precision,
    detector_type character(50),
    res double precision,
    ampt double precision,
    lsec double precision,
    fs double precision,
    date character(50),
    quantification_method character(50),
    quantification_standard character(50),
    quantification_type character(50),
    sem_user character(300)
);
 +   DROP TABLE public.sem_instrument_metadata;
       public         heap    hbrooks    false            �            1259    18126    sem_interference    TABLE     =  CREATE TABLE public.sem_interference (
    sem_ifr_id integer NOT NULL,
    sem_im_id integer NOT NULL,
    sample_id integer NOT NULL,
    element character(50),
    net_interference double precision,
    interference_error double precision,
    background_interference double precision,
    p_b double precision
);
 $   DROP TABLE public.sem_interference;
       public         heap    hbrooks    false            �            1259    18135    tube    TABLE     A  CREATE TABLE public.tube (
    tube_id integer NOT NULL,
    tube_name character(50) NOT NULL,
    depth_top double precision,
    depth_bottom double precision,
    length double precision,
    core_diameter_cm double precision,
    density double precision,
    top_we double precision,
    core_id integer NOT NULL
);
    DROP TABLE public.tube;
       public         heap    hbrooks    false            �           2606    18139    cfa CFA_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.cfa
    ADD CONSTRAINT "CFA_pkey" PRIMARY KEY (cfa_id);
 8   ALTER TABLE ONLY public.cfa DROP CONSTRAINT "CFA_pkey";
       public            hbrooks    false    209            �           2606    18141    icpms ICPMS_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.icpms
    ADD CONSTRAINT "ICPMS_pkey" PRIMARY KEY (icpms_id);
 <   ALTER TABLE ONLY public.icpms DROP CONSTRAINT "ICPMS_pkey";
       public            hbrooks    false    211            �           2606    18143 
   ic IC_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY public.ic
    ADD CONSTRAINT "IC_pkey" PRIMARY KEY (ic_id);
 6   ALTER TABLE ONLY public.ic DROP CONSTRAINT "IC_pkey";
       public            hbrooks    false    210            �           2606    18145    iso ISO_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.iso
    ADD CONSTRAINT "ISO_pkey" PRIMARY KEY (iso_id);
 8   ALTER TABLE ONLY public.iso DROP CONSTRAINT "ISO_pkey";
       public            hbrooks    false    212            �           2606    20183 (   laicpms_chemistry LAICPMS_Chemistry_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.laicpms_chemistry
    ADD CONSTRAINT "LAICPMS_Chemistry_pkey" PRIMARY KEY (laicpms_id);
 T   ALTER TABLE ONLY public.laicpms_chemistry DROP CONSTRAINT "LAICPMS_Chemistry_pkey";
       public            hbrooks    false    221            �           2606    18147 <   laicpms_instrument_metadata LAICPMS_Instrument_Metadata_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.laicpms_instrument_metadata
    ADD CONSTRAINT "LAICPMS_Instrument_Metadata_pkey" PRIMARY KEY (laicpms_run_id);
 h   ALTER TABLE ONLY public.laicpms_instrument_metadata DROP CONSTRAINT "LAICPMS_Instrument_Metadata_pkey";
       public            hbrooks    false    213            �           2606    18149     sem_chemistry SEM_Chemistry_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.sem_chemistry
    ADD CONSTRAINT "SEM_Chemistry_pkey" PRIMARY KEY (sem_chem_id);
 L   ALTER TABLE ONLY public.sem_chemistry DROP CONSTRAINT "SEM_Chemistry_pkey";
       public            hbrooks    false    214            �           2606    18151 4   sem_instrument_metadata SEM_Instrument_Metadata_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.sem_instrument_metadata
    ADD CONSTRAINT "SEM_Instrument_Metadata_pkey" PRIMARY KEY (sem_im_id);
 `   ALTER TABLE ONLY public.sem_instrument_metadata DROP CONSTRAINT "SEM_Instrument_Metadata_pkey";
       public            hbrooks    false    215            �           2606    18153 &   sem_interference SEM_Interference_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.sem_interference
    ADD CONSTRAINT "SEM_Interference_pkey" PRIMARY KEY (sem_ifr_id);
 R   ALTER TABLE ONLY public.sem_interference DROP CONSTRAINT "SEM_Interference_pkey";
       public            hbrooks    false    216            �           2606    18155    core core_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.core
    ADD CONSTRAINT core_pkey PRIMARY KEY (core_id);
 8   ALTER TABLE ONLY public.core DROP CONSTRAINT core_pkey;
       public            hbrooks    false    217            �           2606    18157    sample sample_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.sample
    ADD CONSTRAINT sample_pkey PRIMARY KEY (sample_id);
 <   ALTER TABLE ONLY public.sample DROP CONSTRAINT sample_pkey;
       public            hbrooks    false    218            �           2606    18159    tube tube_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.tube
    ADD CONSTRAINT tube_pkey PRIMARY KEY (tube_id);
 8   ALTER TABLE ONLY public.tube DROP CONSTRAINT tube_pkey;
       public            hbrooks    false    219            �           2606    18160    sample CoreID    FK CONSTRAINT     |   ALTER TABLE ONLY public.sample
    ADD CONSTRAINT "CoreID" FOREIGN KEY (core_id) REFERENCES public.core(core_id) NOT VALID;
 9   ALTER TABLE ONLY public.sample DROP CONSTRAINT "CoreID";
       public          hbrooks    false    218    217    3232            �           2606    18165    icpms ICPMS_SampleID_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.icpms
    ADD CONSTRAINT "ICPMS_SampleID_fkey" FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id) NOT VALID;
 E   ALTER TABLE ONLY public.icpms DROP CONSTRAINT "ICPMS_SampleID_fkey";
       public          hbrooks    false    3234    218    211            �           2606    18170    ic IC_SampleID_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ic
    ADD CONSTRAINT "IC_SampleID_fkey" FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id) NOT VALID;
 ?   ALTER TABLE ONLY public.ic DROP CONSTRAINT "IC_SampleID_fkey";
       public          hbrooks    false    3234    210    218            �           2606    18175    iso ISO_SampleID_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.iso
    ADD CONSTRAINT "ISO_SampleID_fkey" FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id) NOT VALID;
 A   ALTER TABLE ONLY public.iso DROP CONSTRAINT "ISO_SampleID_fkey";
       public          hbrooks    false    3234    212    218            �           2606    18185    sem_interference SEM.IM.ID    FK CONSTRAINT     �   ALTER TABLE ONLY public.sem_interference
    ADD CONSTRAINT "SEM.IM.ID" FOREIGN KEY (sem_im_id) REFERENCES public.sem_instrument_metadata(sem_im_id) NOT VALID;
 F   ALTER TABLE ONLY public.sem_interference DROP CONSTRAINT "SEM.IM.ID";
       public          hbrooks    false    215    3228    216            �           2606    18190    sem_chemistry SEM.IM.ID    FK CONSTRAINT     �   ALTER TABLE ONLY public.sem_chemistry
    ADD CONSTRAINT "SEM.IM.ID" FOREIGN KEY (sem_im_id) REFERENCES public.sem_instrument_metadata(sem_im_id) NOT VALID;
 C   ALTER TABLE ONLY public.sem_chemistry DROP CONSTRAINT "SEM.IM.ID";
       public          hbrooks    false    215    3228    214            �           2606    18200     sem_instrument_metadata SampleID    FK CONSTRAINT     �   ALTER TABLE ONLY public.sem_instrument_metadata
    ADD CONSTRAINT "SampleID" FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id) NOT VALID;
 L   ALTER TABLE ONLY public.sem_instrument_metadata DROP CONSTRAINT "SampleID";
       public          hbrooks    false    3234    215    218            �           2606    18205    sem_interference SampleID    FK CONSTRAINT     �   ALTER TABLE ONLY public.sem_interference
    ADD CONSTRAINT "SampleID" FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id) NOT VALID;
 E   ALTER TABLE ONLY public.sem_interference DROP CONSTRAINT "SampleID";
       public          hbrooks    false    3234    218    216            �           2606    18210    sem_chemistry SampleID    FK CONSTRAINT     �   ALTER TABLE ONLY public.sem_chemistry
    ADD CONSTRAINT "SampleID" FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id) NOT VALID;
 B   ALTER TABLE ONLY public.sem_chemistry DROP CONSTRAINT "SampleID";
       public          hbrooks    false    214    218    3234            �           2606    18215    cfa SampleID    FK CONSTRAINT     �   ALTER TABLE ONLY public.cfa
    ADD CONSTRAINT "SampleID" FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id) NOT VALID;
 8   ALTER TABLE ONLY public.cfa DROP CONSTRAINT "SampleID";
       public          hbrooks    false    209    3234    218            �           2606    18225    sample TubeID    FK CONSTRAINT     |   ALTER TABLE ONLY public.sample
    ADD CONSTRAINT "TubeID" FOREIGN KEY (tube_id) REFERENCES public.tube(tube_id) NOT VALID;
 9   ALTER TABLE ONLY public.sample DROP CONSTRAINT "TubeID";
       public          hbrooks    false    219    3236    218            �           2606    18230    tube tube_CoreID_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tube
    ADD CONSTRAINT "tube_CoreID_fkey" FOREIGN KEY (core_id) REFERENCES public.core(core_id) NOT VALID;
 A   ALTER TABLE ONLY public.tube DROP CONSTRAINT "tube_CoreID_fkey";
       public          hbrooks    false    217    3232    219           